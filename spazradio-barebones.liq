set("log.file.path","/tmp/test-radio.log")


def orn(a,b) 
  if a != "" then
    a 
  else 
    b 
  end
end

# Pattern used everywhere in Airtime: for side effects or sinking, 
# send output of a stream to an ignored output.dummy
# @category Liquidsoap
# @param ~s A source
def dummy_up(s)
  ignore(output.dummy(fallible=true, s))
end

def log2_metadata(msg, m)
  mj = json_of(m)
  log(msg ^ " now: #{mj}")
end



# hacks to get around bug in 1.1.1 where skip_blank was removed and not replaced or documnted!!


# Skip track when detecting a blank.
# @category Source / Track Processing
# @param ~id Force the value of the source ID.
# @param ~threshold Power in decibels under which the stream is considered silent.
# @param ~max_blank Maximum silence length allowed, in seconds.
# @param ~min_noise Minimum duration of noise required to end silence, in seconds.
# @param ~track_sensitive Reset blank counter at each track.

def skip_blank(~id="", ~threshold=-40., ~max_blank=20., ~track_sensitive=true, s)
  on_blank({source.skip(s)}, threshold=threshold, length=max_blank, track_sensitive=track_sensitive, s)
end



live_meta = ref []

def munge_live_hdr(m)
  log("got headers #{m}")
  # not setting title here because it might get set by something else
  lm =  [("artist_clean", orn(m['ice-name'], m['icy-name'])),
         ("artist", "[LIVE!] " ^ 
          orn(m['ice-name'], m['icy-name'] ) 
          ^ " - " ^ 
          orn(m['ice-description']  , m['icy-description'] )),
         ("mapped", "true"), # make airtime stop attempting to mangle it
         ("description",  
          orn(m['ice-description'], m['icy-description']))]
  log("set live_meta to #{lm}")
  lm
end



def master_dj_connect(header) 
  live_meta := munge_live_hdr(header)
end

def master_dj_disconnect() 
  live_meta := []
end


def legal_name_fix(s)
  string.replace(pattern='[^abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789]+',
                 fun (_) -> '_',s)
end

def format_save(m)
  [("title", list.nth(get_process_lines("date '+%Y-%m-%d'"),0)),
   ('album', m['artist_clean']),
   ('artist', legal_name_fix(m['artist_clean']))]
end


silence = amplify(id="silence_src", 0.00001, noise())
ignore(output.dummy(silence, fallible=true))

# check both drop and imported locations, 
# just in case something screws up via airtime
jukebox = 
  audio_to_stereo(id="stereoize",
                  random(id="randomize-plaaylists",
                         [skip_blank(id="stor-skipblank",
                                     playlist(id="stor-dir", 
                                              reload=86400, 
                                              "/home/ken/tmp/spazbackup"))]))


master_raw =
  audio_to_stereo(
                  input.harbor("stream",
                               port=8020,
                               password="",
                               logfile="/tmp/master-buffer.log",
                               on_connect=master_dj_connect,
                               on_disconnect=master_dj_disconnect,
                               max=40.))



# we don't want dj's leaving their system on and forgetting...
master_dj = strip_blank(id="master-stripblank",
                        threshold=-30.0,
                        length=60.0,
                        master_raw)


dummy_up(master_dj)

pair = insert_metadata(drop_metadata(master_dj))
master_metadata_f = fst(pair)
master_meta = snd(pair)


spair = insert_metadata(drop_metadata(master_dj))
save_metadata_f = fst(spair)
master_save = snd(spair)


def force_live_metadata(_)
  ignore(master_metadata_f(!live_meta))
end


log2_metadata("SAVE metadata before output", master_save)

master_save_buffer = buffer(id="master_save_buffer",
                            fallible=true, 
                            on_start=fun()->ignore(save_metadata_f(format_save(!live_meta))),
                            on_stop=fun()->ignore(save_metadata_f([])),
                            master_save)


output.file(%vorbis,
            fallible=true,
            append=false,
            reopen_on_metadata=true,
            reopen_delay=30.0,
            "/home/streams/%Y-%m-%d-%H_%M_%S-$(artist).ogg",
            master_save_buffer)


dummy_up(on_metadata(force_live_metadata, master_dj))
dummy_up(on_track(force_live_metadata, master_dj))

master_buffer = buffer(id="master_dj_buffer",
                       fallible=true,
                       on_start=fun()->ignore(master_metadata_f(!live_meta)),
                       on_stop=fun()->ignore(save_metadata_f([])),
                       master_meta)




default = fallback(id="default-switcher",
                   track_sensitive=false,
                   [master_buffer,
                    jukebox,
                    silence])


output.icecast(
               %vorbis(quality=0.5, channels = 2),
               mount="/radio.ogg",
               fallible=true,
               host="localhost", port=8050, password="",
               default)

output.icecast(
               %vorbis(quality=0.1, channels = 2),
               mount="/radio-low.ogg",
               fallible=true,
               host="localhost", port=8050, password="",
               default)

output.icecast(
               %mp3(bitrate=128, stereo= true),
               mount="/radio",
               fallible=true,
               host="localhost", port=8050, password="",
               default)

